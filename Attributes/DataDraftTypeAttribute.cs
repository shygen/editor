﻿#region Disclaimer
// <copyright file="DataDraftTypeAttribute.cs">
// Copyright (c) 2017 - 2017 All Rights Reserved
// </copyright>
// <author>Robin Fischer</author>
#endregion
namespace Shygen.Editor.Attributes
{
    using System;

    /// <summary>
    /// Placed on a category it tells shygen which DataDraft this category uses
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class DataDraftTypeAttribute : Attribute
    {
        public Type Type { get; private set; }

        public DataDraftTypeAttribute(Type type)
        {
            Type = type;
        }
    }
}