﻿#region Disclaimer
// <copyright file="RuntimeAttributeTypeAttribute.cs">
// Copyright (c) 2017 - 2017 All Rights Reserved
// </copyright>
// <author>Robin Fischer</author>
#endregion
namespace Shygen.Editor.Attributes
{
    using System;

    /// <summary>
    /// Is placed on the category to specify the runtime attribute which the runtime
    /// uses to tell shygen with which category to generate code for this class
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class RuntimeAttributeTypeAttribute : Attribute
    {
        public Type Type { get; private set; }

        public RuntimeAttributeTypeAttribute(Type type)
        {
            Type = type;
        }
    }
}