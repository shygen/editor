namespace Shygen.Editor
{
    public interface ICodeGenerationEditorSettings
    {
        /// <summary>
        /// Control if Roslyn should be used to gather information about the types to generate. Making this a setting
        /// because not all machines can execute the Roslyn process or have the lib installed. If this is disabled you
        /// have to always have correctly compiling code
        /// </summary>
        bool UseRoslyn { get; set; }

        /// <summary>
        /// The path to the dotnet core analyzer executable. Usually located in Shygen.Editor under 'Shygen/Editor/Roslyn~/RoslynSyntaxWalker/final/netcoreapp2.2/RoslynSyntaxWalker.dll'
        /// </summary>
        string AnalyzerPath { get; set; }

        string DotnetPath { get; set; }
    }
}