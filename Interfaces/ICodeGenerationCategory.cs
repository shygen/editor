﻿#region Disclaimer

// <copyright file="ICodeGenerationCategory.cs">
// Copyright (c) 2016 - 2017 All Rights Reserved
// </copyright>
// <author>Robin Fischer</author>

#endregion

namespace Shygen.Editor.Interfaces
{
    using System;

    /// <summary>
    /// This can be implemented to add custom generation categories.
    /// Shygen is fully flexible what kind of categories are implemented.
    /// This allows to generate different parts of a pattern.
    /// For example MVC (Model-View-Controller) The Mode, View and Controller
    /// are separate categories because they have to be generated differently.
    /// But all Models use the same generators and templates to generate the code.
    /// </summary>
    public interface ICodeGenerationCategory
    {
        string Name { get; }
        string Namespace { get; }
        Type DataDraftType { get; }
        Type RuntimeAttributeType { get; }
    }
}