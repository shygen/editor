﻿#region Disclaimer

// <copyright file="CodeGenerationSettings.cs">
// Copyright (c) 2016 - 2017 All Rights Reserved
// </copyright>
// <author>Robin Fischer</author>

#endregion

using Shygen.Runtime.Attributes;

namespace Shygen.Editor
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Generation settings on which modules and templates to use
    /// </summary>
    public class CodeGenerationSettings
    {
        public List<CodeGenerationCategorySetting> CategorySettings { get; set; }

        public List<ModuleGroupSetting> ModuleGroupSettings { get; set; }

        public CodeGenerationCategorySetting GetSettingsForCategory(string categoryName)
        {
            if (CategorySettings != null)
            {
                for (var i = 0; i < CategorySettings.Count; i++)
                {
                    CodeGenerationCategorySetting categorySetting = CategorySettings[i];
                    if (categorySetting.CategoryName == categoryName)
                    {
                        return categorySetting;
                    }
                }
            }
            return null;
        }

        public ModuleGroupSetting GetModuleGroup(string modulelPath)
        {
            for (var i = 0; i < ModuleGroupSettings.Count; i++)
            {
                ModuleGroupSetting group = ModuleGroupSettings[i];

                for (var j = 0; j < group.ModulePaths.Count; j++)
                {
                    string path = group.ModulePaths[j];

                    if (path.Equals(modulelPath))
                    {
                        return group;
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// A module group contains multiple modules. This allows a logical separation
        /// between different code namespaces. Framework and Game for example.
        /// </summary>
        public class ModuleGroupSetting
        {
            public string Name { get; set; }

            public string Namespace { get; set; }

            public List<string> ModulePaths { get; private set; }

            /// <summary>
            /// Path to directory where generated code for this module is placed.
            /// </summary>
            public string TargetDirectoryPath { get; set; }

            public string Scope { get; set; }

            public ModuleGroupSetting()
            {
                ModulePaths = new List<string>();
            }
        }

        /// <summary>
        /// Defines which templates should be used per category in the generation process
        /// </summary>
        public class CodeGenerationCategorySetting
        {
            public CodeGenerationCategorySetting()
            {
                Settings = new List<CodeGenerationSetting>();
            }

            public string CategoryName { get; set; }

            public List<CodeGenerationSetting> Settings { get; set; }
        }

        /// <summary>
        /// Class to find a template file
        /// </summary>
        public class CodeGenerationSetting
        {
            public TemplateConfiguration Path { get; set; }
        }
    }
}