using System.Collections.Generic;
using Shygen.Editor.Interfaces;

namespace Shygen.Editor.Generators
{
    /// <summary>
    /// This is the simplest <see cref="AbstractCSharpGenerator"/>.
    /// Use this generator to generate one file per <see cref="DataDraft"/>
    /// </summary>
    public abstract class CSharpSingleGenerator : AbstractCSharpGenerator
    {
        public override void Generate(string moduleName, string moduleNamespace, ICodeGenerationCategory category,
            DataDraft[] dataDrafts,
            string scope, string group,
            out GeneratedCodeResult[] code)
        {
            base.Generate(moduleName, moduleNamespace, category, dataDrafts, scope, group, out code);

            var results = new List<GeneratedCodeResult>();
            for (var i = 0; i < dataDrafts.Length; i++)
            {
                DataDraft dataDraft = dataDrafts[i];

                bool modifyResult = ModifyTemplate(Template, dataDraft);

                if (modifyResult)
                {
                    var result = new GeneratedCodeResult(GetFileName(dataDraft), Template.Output(), UseGenerationHeader());
                    results.Add(result);
                }
                Template.Reset();
            }
            code = results.ToArray();
        }

        protected abstract string GetFileName(DataDraft data);

        /// <summary>
        /// Return if the code should be decorated with a header which specifies that is generated
        /// Disabling this is useful for some file formats that don't allow comments (JSON/YAML/etc.)
        /// </summary>
        protected virtual bool UseGenerationHeader()
        {
            return true;
        }

        /// <summary>
        /// Must be overriden to modify the template to the <see cref="DataDraft"/>.
        /// Can return different values to only generate a file when needed.
        /// </summary>
        /// <returns>True if a generated file should be created. False if no file should be generated.</returns>
        protected abstract bool ModifyTemplate(CSharpTemplate template, DataDraft data);
    }

    public abstract class CSharpSingleGenerator<TDataDraft> : CSharpSingleGenerator where TDataDraft : DataDraft
    {
        protected sealed override bool ModifyTemplate(CSharpTemplate template, DataDraft data)
        {
            if (data is not TDataDraft)
            {
                // Our generator can't handle the data draft format
                return false;
            }
            return ModifyTemplate(template, (TDataDraft) data);
        }

        protected abstract bool ModifyTemplate(CSharpTemplate template, TDataDraft data);

        protected sealed override string GetFileName(DataDraft data)
        {
            return GetFileName((TDataDraft) data);
        }

        protected abstract string GetFileName(TDataDraft data);
    }
}