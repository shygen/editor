﻿using System;

namespace Shygen.Editor.Generators
{
    using System.Text;
    using Interfaces;
    using RobinBird.Logging.Runtime;
    using Runtime.CSharpGeneration;

    /// <summary>
    /// Contains logic for all operations performed on the template file.
    /// This class can be inherited to make custom methods that are then
    /// available in the template file. Which template is used can be specified
    /// in the template file itself. Look into documentation for details.
    /// </summary>
    public abstract class InlineTemplate : Template
    {
        private readonly StringBuilder codeBuilder = new StringBuilder();

        protected string ModuleName;

        protected string ModuleNamespace;

        protected ICodeGenerationCategory Category;

        protected DataDraft Data { get; set; }
        protected DataDraft[] Datas { get; set; }

        public string Generate(string moduleName, string moduleNamespace, ICodeGenerationCategory category, DataDraft data)
        {
            ModuleName = moduleName;
            ModuleNamespace = moduleNamespace;
            Category = category;
            Initialize(data);
            InternalGenerate();
            return codeBuilder.ToString();
        }

        public string GenerateBatch(string moduleName, string moduleNamespace, ICodeGenerationCategory category, DataDraft[] datas)
        {
            ModuleName = moduleName;
            ModuleNamespace = moduleNamespace;
            Category = category;
            InitializeBatch(datas);
            InternalGenerate();
            return codeBuilder.ToString();
        }

        protected virtual void Initialize(DataDraft dataDraft)
        {
            if (dataDraft == null)
            {
                Log.Warn("Could not initialize null data draft.");
                return;
            }
            codeBuilder.Length = 0;
            Data = dataDraft;
            Datas = new[] {dataDraft};
        }

        protected virtual void InitializeBatch(DataDraft[] dataDrafts)
        {
            if (dataDrafts.Length == 0)
            {
                Log.Warn("Generation batch has zero entries.");
                return;
            }
            codeBuilder.Length = 0;
            Data = dataDrafts[0];
            Datas = dataDrafts;
        }

        protected virtual void InternalGenerate()
        {
        }

        protected virtual void Output(string value)
        {
            codeBuilder.Append(value);
        }

        #region Helpers

        protected void OutputDataName()
        {
            Output(Data.Name);
        }

        protected void OutputPropertyType(Property prop)
        {
            Output(prop.ToCSharpType());
        }

        protected void OutputComponentDraftType(DataDraft dataDraft)
        {
            Output(dataDraft.Namespace + "." + dataDraft.Name);
        }

        #endregion
    }

    public abstract class InlineTemplate<TDataDraft> : InlineTemplate where TDataDraft : DataDraft
    {
        protected TDataDraft Data
        {
            get { return (TDataDraft) base.Data; }
        }

        protected TDataDraft[] Datas
        {
            get { return Array.ConvertAll(base.Datas, input => (TDataDraft) input); }
        }
    }
}