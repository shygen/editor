using System;
using System.Collections.Generic;
using Shygen.Editor.Interfaces;

namespace Shygen.Editor.Generators
{
    /// <summary>
    /// Use if you want to create multiple files per <see cref="DataDraft"/>
    /// </summary>
    public abstract class CSharpMultiGenerator : AbstractCSharpGenerator
    {
        public class Specification
        {
            public string Name { get; }

            public Specification(string name)
            {
                Name = name;
            }
        }

        public override void Generate(string moduleName, string moduleNamespace, ICodeGenerationCategory category,
            DataDraft[] dataDrafts,
            string scope, string group,
            out GeneratedCodeResult[] code)
        {
            base.Generate(moduleName, moduleNamespace, category, dataDrafts, scope, group, out code);

            var results = new List<GeneratedCodeResult>();
            for (var i = 0; i < dataDrafts.Length; i++)
            {
                DataDraft dataDraft = dataDrafts[i];

                foreach (Specification specification in GetFileSpecifications(dataDraft))
                {
                    bool modifyResult = ModifyTemplate(Template, dataDraft, specification);

                    if (modifyResult)
                    {
                        var result = new GeneratedCodeResult(specification.Name, Template.Output());
                        results.Add(result);
                    }
                    Template.Reset();
                }
            }
            code = results.ToArray();
        }

        protected abstract bool ModifyTemplate(CSharpTemplate template, DataDraft dataDraft, Specification specification);

        /// <summary>
        /// Returns how many files should be generated per given <see cref="DataDraft"/>
        /// </summary>
        protected abstract Specification[] GetFileSpecifications(DataDraft dataDraft);
    }

    public abstract class CSharpMultiGenerator<TDataDraft> : CSharpMultiGenerator where TDataDraft : DataDraft
    {
        protected sealed override Specification[] GetFileSpecifications(DataDraft dataDraft)
        {
            if (dataDraft.GetType() != typeof(TDataDraft))
            {
                // Our generator can't handle the data draft format
                return Array.Empty<Specification>();
            }
            return GetFileSpecifications((TDataDraft) dataDraft);
        }

        protected abstract Specification[] GetFileSpecifications(TDataDraft dataDraft);

        protected sealed override bool ModifyTemplate(CSharpTemplate template, DataDraft dataDraft, Specification specification)
        {
            if (dataDraft is not TDataDraft)
            {
                // Our generator can't handle the data draft format
                return false;
            }
            return ModifyTemplate(template, (TDataDraft) dataDraft, specification);
        }

        protected abstract bool ModifyTemplate(CSharpTemplate template, TDataDraft dataDraft, Specification specification);
    }
}