using System;
using Shygen.Editor.Interfaces;

namespace Shygen.Editor.Generators
{
    /// <summary>
    /// Use this if you want to output one file for all <see cref="DataDraft"/>s per given module and category
    /// Example could be that you want to create one file which contains constants of all class names inside of a module
    /// </summary>
    public abstract class CSharpBatchGenerator : AbstractCSharpGenerator
    {
        public override void Generate(string moduleName, string moduleNamespace, ICodeGenerationCategory category,
            DataDraft[] dataDrafts,
            string scope, string group,
            out GeneratedCodeResult[] code)
        {
            base.Generate(moduleName, moduleNamespace, category, dataDrafts, scope, group, out code);

            if (ModifyTemplate(Template, dataDrafts))
            {
                code = new[] {new GeneratedCodeResult(GetFileName(), Template.Output())};
            }
            else
            {
                code = new GeneratedCodeResult[0];
            }
        }

        /// <summary>
        /// Filename without extension
        /// </summary>
        protected abstract string GetFileName();

        protected abstract bool ModifyTemplate(CSharpTemplate template, DataDraft[] data);
    }

    public abstract class CSharpBatchGenerator<TDataDraft> : CSharpBatchGenerator where TDataDraft : DataDraft
    {
        protected sealed override bool ModifyTemplate(CSharpTemplate template, DataDraft[] data)
        {
            if (data[0] is not TDataDraft)
            {
                // Our generator can't handle the data draft format
                return false;
            }
            return ModifyTemplate(template, Array.ConvertAll(data, input => (TDataDraft) input));
        }

        protected abstract bool ModifyTemplate(CSharpTemplate template, TDataDraft[] dataDrafts);
    }
}