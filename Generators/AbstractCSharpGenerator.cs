﻿
namespace Shygen.Editor.Generators
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text.RegularExpressions;
    using System.Xml;
    using Interfaces;
    using RobinBird.Logging.Runtime;
    using RobinBird.Utilities.Runtime.Extensions;
    using Runtime.Attributes;

    /// <summary>
    /// Inline code template. A normal class is written in compiled C# code. The
    /// code is then decorated with annotations that help in the generation process.
    /// The generator can perform operations on the template class to get the generated
    /// result. This is very usefull because the template class is compiler tracked and
    /// survives refactoring and code reference checking. You also get syntax highlighting
    /// and immediate error checking in templates.
    /// </summary>
    public abstract class AbstractCSharpGenerator : Generator
    {
        public const string Token = "//%";
        public const string GroupToken = "group";

        protected CSharpTemplate Template;
        
        /// <summary>
        /// In which area of the application this generator must operate (e.g. frontend, shared, backend, etc.)
        /// Check <see cref="TemplateConfiguration"/> for scope constants
        /// </summary>
        protected string Scope;

        /// <summary>
        /// Name of the group. (e.g. Game or ECS)
        /// </summary>
        protected string GroupName;

        protected string ModuleName { get; private set; }

        protected ICodeGenerationCategory Category { get; private set; }

        /// <summary>
        /// Only used for generated classes that have no direct generation source
        /// If you are generating from a partial class always use <see cref="DataDraft.Namespace"/>
        /// </summary>
        protected string ModuleNamespace { get; private set; }

        /// <summary>
        /// Namespace which is valid across all modules within one group. Groups are "Game" and "ECS" for example 
        /// </summary>
        protected string GroupNamespace =>
            ModuleNamespace.Remove(ModuleNamespace.LastIndexOf(".", StringComparison.InvariantCulture));

        public override void Initialize(FileInfo template)
        {
            // Get Name
            Type type = GetType();
            var nameAttribute = type.GetCustomAttribute<TemplateNameAttribute>(false);

            if (nameAttribute == null)
            {
                nameAttribute = new TemplateNameAttribute(template.Name);
            }

            TemplateName = nameAttribute.Name;

            // Get optional extension
            var extensionAttribute = type.GetCustomAttribute<TemplateExtensionAttribute>(false);

            if (extensionAttribute != null)
            {
                Extension = extensionAttribute.Extension;
            }

            InitializeCode(template);
        }

        public override void Generate(string moduleName, string moduleNamespace, ICodeGenerationCategory category,
            DataDraft[] dataDrafts, string scope, string group, out GeneratedCodeResult[] code)
        {
            Scope = scope;
            GroupName = group;
            ModuleName = moduleName;
            ModuleNamespace = moduleNamespace;
            Category = category;

            code = null;
        }

        private void InitializeCode(FileInfo templateFile)
        {
            string text = File.ReadAllText(templateFile.FullName);

            // Find Template marks
            const string start = @"\<";
            const string end = @"\>";

            const string templateToken = "template";
            // TODO: remove the .* when Generator assignment is cleaner.
            // Insert .* for attributes
            const string templateStart = Token + start + templateToken + ".*" + end;
            const string templateEnd = start + "/" + templateToken + end;
            const string templateGroupName = templateToken;

            var templateRegex = new Regex(string.Format(@"{0}(?<{1}>.*){2}", templateStart, templateGroupName, templateEnd), RegexOptions.Singleline);

            Match match = templateRegex.Match(text);

            if (match.Success == false)
            {
                throw new InvalidOperationException($"Could not find Template code in '{templateFile}'. Please check if the tags '{templateStart}' and '{templateEnd}' are placed in the comments.");
            }

            string templateText = match.Value;

            // Find special symbols and escape them
            var symbolLowerThanRegex = new Regex(@"(?<!%)<");
            var symbolGreaterThanRegex = new Regex(@"(?<!"")>");

            templateText = templateText.Replace("&", "&amp;");
            templateText = symbolLowerThanRegex.Replace(templateText, "&lt;");
            //templateText = symbolGreaterThanRegex.Replace(templateText, "&gt;");

            // remove tokens
            string normalizedText = templateText.Replace(Token, string.Empty);


            var d = new XmlDocument();
            try
            {
                d.LoadXml(normalizedText);
            }
            catch (Exception e)
            {
#if UNITY_5_3_OR_NEWER
                UnityEditor.EditorGUIUtility.systemCopyBuffer = normalizedText;
#endif
                Log.Info("Xml text: " + normalizedText
                             .Replace("<", "__")
                             .Replace(">","--")
                             .Replace("{","..")
                             .Replace("}", "::"));
                throw e;
            }


            const string groupNameAttributeToken = "name";

            var groups = new Dictionary<string, CSharpTemplate.Group>();

            XmlNode templateNode = d.ChildNodes[0];
            string innerTemplateText = templateNode.InnerXml;

            for (var i = 0; i < templateNode.ChildNodes.Count; i++)
            {
                XmlNode child = templateNode.ChildNodes[i];

                if (child.Name != GroupToken)
                {
                    continue;
                }

                // Found a group
                if (child.HasAttribute(groupNameAttributeToken) == false)
                {
                    throw new InvalidDataException(string.Format("Could not find '{0}' Attribute.", groupNameAttributeToken));
                }

                // ReSharper disable once PossibleNullReferenceException
                string name = child.Attributes[groupNameAttributeToken].Value;

                string groupText = child.InnerText;
                // Remove leading and trailing whitespace
                groupText = ReplaceSpecialXmlSymbols(groupText).Trim();

                var group = new CSharpTemplate.Group(groupText);
                if (groups.ContainsKey(name) == false)
                    groups.Add(name, @group);
                else
                    Log.Error($"Duplicate group ({name}) in template {templateFile.FullName}");

                // Replace group in template
                innerTemplateText = innerTemplateText.Replace(child.OuterXml, GetGroupNameToken(name));
            }

            innerTemplateText = ReplaceSpecialXmlSymbols(innerTemplateText);

            Template = new CSharpTemplate(innerTemplateText, groups);
        }

        private static string ReplaceSpecialXmlSymbols(string text)
        {
            return text.Replace("&gt;", ">")
                .Replace("&lt;", "<")
                .Replace("&amp;", "&");
        }

        public static string GetGroupNameToken(string name)
        {
            return Token + GroupToken + "=" + name + Token;
        }
    }
}