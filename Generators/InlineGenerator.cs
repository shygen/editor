﻿namespace Shygen.Editor.Generators
{
    using System;
    using System.CodeDom.Compiler;
    using System.Collections.Generic;
    using System.IO;
    using System.Reflection;
    using System.Text;
    using System.Text.RegularExpressions;
    using Interfaces;
    using Microsoft.CSharp;
    using RobinBird.Logging.Runtime;
    using RobinBird.Utilities.Runtime.Extensions;

    /// <summary>
    /// The more traditional file base template generation. You have a seperate ".tat" file
    /// that contains all template and inline code information. The content of the template
    /// file are injected and temporarily compiled with <see cref="CSharpCodeProvider" /> to be executed.
    /// </summary>
    public class InlineGenerator : Generator
    {
        private static CSharpCodeProvider _compiler;
        private static string[] _currentAssemblies;

        private string namePostfix = string.Empty;
        protected InlineTemplate Generator { get; set; }

        /// <summary>
        /// Mode of how data is injected into the template
        /// </summary>
        public enum IterationMode
        {
            /// <summary>
            /// The generation process is called for each
            /// <see cref="DataDraft" /> over and over. This is useful
            /// because you just have to deal with one data at the time and
            /// don't have to encapsulate everything in for loops.
            /// </summary>
            Single,

            /// <summary>
            /// The generation process is called once with an array of
            /// <see cref="DataDraft" />. This is usefull if the template
            /// needs information from other data drafts than its own. The total
            /// count of data drafts, for example.
            /// </summary>
            Batch,
        }

        /// <summary>
        /// The iteration mode that is applied during generation
        /// </summary>
        public IterationMode Iteration { get; set; }


        public string NamePostfix
        {
            get { return namePostfix; }
            set { namePostfix = value; }
        }

        /// <summary>
        /// Compiler used for compiling generators which generate result code
        /// </summary>
        private static CSharpCodeProvider Compiler
        {
            get
            {
                if (_compiler == null)
                {
                    var providerOptions = new Dictionary<string, string>();
                    providerOptions.Add("CompilerVersion", "v4.0");
                    _compiler = new CSharpCodeProvider(providerOptions);
                }
                return _compiler;
            }
        }

        /// <summary>
        /// Assemblies used for compiling the generators
        /// </summary>
        private static string[] CurrentAssemblies
        {
            get
            {
                if (_currentAssemblies != null)
                    return _currentAssemblies;

                var assemblies = new List<string>();
                foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
                {
                    try
                    {
                        string location = assembly.Location;
                        if (string.IsNullOrEmpty(location) == false)
                        {
                            assemblies.Add(location);
                        }
                    }
                    catch (NotSupportedException)
                    {
                        // this happens for dynamic assemblies, so just ignore it.
                    }
                }
                _currentAssemblies = assemblies.ToArray();
                return _currentAssemblies;
            }
        }

        public override void Generate(string moduleName, string moduleNamespace, ICodeGenerationCategory category,
            DataDraft[] dataDrafts, string scope, string group, out GeneratedCodeResult[] code)
        {
            if (Generator == null)
            {
                throw new InvalidOperationException("The generator was not created. Please run Initialize() or check if the initialization had errors.");
            }

            if (dataDrafts.Length == 0)
            {
                // Not generating anything because data is empty.
                code = new GeneratedCodeResult[0];
                return;
            }

            switch (Iteration)
            {
                case IterationMode.Single:
                    var tmpCode = new GeneratedCodeResult[dataDrafts.Length];
                    for (var i = 0; i < dataDrafts.Length; i++)
                    {
                        DataDraft dataDraft = dataDrafts[i];
                        string singleFileName = string.Format("{0}{1}{2}", dataDraft, TemplateName, NamePostfix);
                        tmpCode[i] = new GeneratedCodeResult(singleFileName, Generator.Generate(moduleName, moduleNamespace, category, dataDraft));
                    }
                    code = tmpCode;
                    break;
                case IterationMode.Batch:
                    string batchFileName = string.Format("{0}{1}{2}", moduleName, TemplateName, NamePostfix);
                    code = new[] {new GeneratedCodeResult(batchFileName, Generator.GenerateBatch(moduleName, moduleNamespace, category, dataDrafts))};
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public override void Initialize(FileInfo template)
        {
            const string escapeStartSymbol = "<";
            const string escapeEndSymbol = ">";

            if (template.Exists == false)
            {
                throw new FileNotFoundException(string.Format("Could not find supplied template. '{0}'", template.FullName), template.FullName);
            }

            // Read file and generate child generator
            string templateContent;

            using (var reader = new StreamReader(template.FullName))
            {
                templateContent = reader.ReadToEnd();
            }

            var code = new StringBuilder();
            var walkingInt = 0;

            #region Config

            // Extract config
            const string configSymbol = @"\$";
            const string configRegexName = "config";

            string configRegexString = string.Format(@"{0}{1}(?<{3}>.*){1}{2}", escapeStartSymbol, configSymbol, escapeEndSymbol, configRegexName);

            var configRegex = new Regex(configRegexString, RegexOptions.Singleline);

            Match configMatch = configRegex.Match(templateContent);

            // Generator Settings
            TemplateName = Path.GetFileNameWithoutExtension(template.Name);
            Type parentGenClass = typeof (InlineTemplate);
            Iteration = IterationMode.Batch;

            if (configMatch.Success)
            {
                if (configMatch.NextMatch().Success)
                {
                    throw new InvalidDataException("Data cannot have multiple config sections.");
                }

                string configValue = configMatch.Groups[configRegexName].Value;

                #region Name

                const string configNameSymbolName = "name";

                string templateName;

                if (GetConfigArgument(configNameSymbolName, configValue, out templateName))
                {
                    TemplateName = templateName;
                }

                #endregion

                #region Provider

                // Get provider information
                // TODO: Rename to Template
                const string configProviderSymbolName = "provider";

                string providerClassName;

                if (GetConfigArgument(configProviderSymbolName, configValue, out providerClassName))
                {
                    Type providerType = AppDomain.CurrentDomain.GetType(providerClassName);
                    if (providerType == null)
                    {
                        throw new InvalidDataException(string.Format("The provider selected by the template could not be found. Provider: {0} Template: {1}", providerClassName,
                            template.FullName));
                    }

                    if (providerType.IsSubclassOf(parentGenClass) == false)
                    {
                        throw new InvalidDataException(string.Format("The provider Type does not inherit from the generator base class. BaseClass: {0} Provider: {1} Template: {2}",
                            parentGenClass, providerType, template.FullName));
                    }
                    parentGenClass = providerType;
                }

                #endregion

                #region Iteration

                const string configIterationSymbolName = "iteration";

                string mode;

                if (GetConfigArgument(configIterationSymbolName, configValue, out mode))
                {
                    IterationMode tmpMode;
                    try
                    {
                        tmpMode = (IterationMode) Enum.Parse(typeof (IterationMode), mode);
                    }
                    catch (Exception e)
                    {
                        throw new NotSupportedException(string.Format("Could not read argument '{0}' for config option '{1}'.", mode, configIterationSymbolName), e);
                    }

                    Iteration = tmpMode;
                }

                #endregion

                #region Postfix

                const string configPostfixSymbolName = "postfix";

                string postfix;

                if (GetConfigArgument(configPostfixSymbolName, configValue, out postfix))
                {
                    NamePostfix = postfix;
                }

                #endregion

                #region Extension

                const string configExtensionSymbolName = "extension";

                string extension;

                if (GetConfigArgument(configExtensionSymbolName, configValue, out extension))
                {
                    Extension = extension;
                }

                #endregion

                // Setting current pointer after the config string.
                walkingInt = configMatch.Index + configMatch.Length;
            }

            #endregion

            #region Using Statement

            const string usingSymbol = @"§";
            const string usingRegexName = "using";

            string usingRegexString = string.Format(@"{0}{1}(?<{3}>.*){1}{2}", escapeStartSymbol, usingSymbol, escapeEndSymbol, usingRegexName);

            var usingRegex = new Regex(usingRegexString, RegexOptions.Singleline);

            Match usingMatch = usingRegex.Match(templateContent);

            if (usingMatch.Success)
            {
                // Found using statement
                if (usingMatch.NextMatch().Success)
                {
                    throw new InvalidDataException("Data cannot have multiple config sections.");
                }
                string usingStatement = usingMatch.Groups[usingRegexName].Value;

                code.Append(usingStatement);
                code.Append(Environment.NewLine);

                // Setting current pointer after the using string.
                walkingInt = usingMatch.Index + usingMatch.Length;
            }

            #endregion

            #region Code

            const string codeSymbol = "%";
            const string codeGroupName = "code";

            var templateString = new StringBuilder();
            var codeRegex = new Regex(string.Format(@"{2}{0}\s*(?<{1}>.*?)\s*{0}{3}", codeSymbol, codeGroupName, escapeStartSymbol, escapeEndSymbol), RegexOptions.Singleline);
            Match codeMatch = codeRegex.Match(templateContent, walkingInt);

            // Go through every match.
            while (codeMatch.Success)
            {
                // Append normal text as Output
                string substring = templateContent.Substring(walkingInt, codeMatch.Index - walkingInt);
                substring = FixNormalOutputText(substring);
                templateString.AppendFormat("Output(@\"{0}\");{1}", substring, Environment.NewLine);

                // Append code
                templateString.AppendFormat("{0}{1}", codeMatch.Groups[codeGroupName].Value, Environment.NewLine);

                walkingInt = codeMatch.Index + codeMatch.Length;
                codeMatch = codeMatch.NextMatch();
            }

            // Append last text
            string lastString = templateContent.Substring(walkingInt, templateContent.Length - walkingInt);
            lastString = FixNormalOutputText(lastString);
            templateString.AppendFormat("Output(@\"{0}\");{1}", lastString, Environment.NewLine);

            #endregion

            TemporaryCompile(template, parentGenClass, code, templateString);
        }

        private void TemporaryCompile(FileInfo template, Type parentGenClass, StringBuilder code, StringBuilder templateString)
        {
            string temporaryClassName = string.Format("Temp{0}", parentGenClass.Name);

            // Write file structure
            code.Append(string.Format(@"
public class {2} : {0}
{{
protected override void InternalGenerate()
{{
base.InternalGenerate();
{1}
}}

}}", parentGenClass.FullName, templateString, temporaryClassName));

            CompilerResults res;

            try
            {
                CSharpCodeProvider provider = Compiler;

                var options = new CompilerParameters(CurrentAssemblies)
                {
                    GenerateExecutable = false,
                    GenerateInMemory = false
                };

                res = provider.CompileAssemblyFromSource(options, code.ToString());

                provider.Dispose();

                if (res.Errors.HasErrors || res.Errors.HasWarnings)
                {
                    // TODO: Make this optional
#if UNITY_EDITOR
                    //UnityEditor.EditorGUIUtility.systemCopyBuffer = code.ToString();
#endif
                    Log.ErrorFormat("Errors in template: '{1}' Copied Generated Generated Code to Clipboard:{2}{0}",new object[]{ code, template.FullName, Environment.NewLine});

                    for (var i = 0; i < res.Errors.Count; i++)
                    {
                        CompilerError error = res.Errors[i];

                        string errorType = error.IsWarning ? "WARNING" : "ERROR";
                        Log.ErrorFormat("{3}:({0},{1}): {2}", new object[]{ error.Line, error.Column, error.ErrorText, errorType});
                    }
                    return;
                }
            }
            catch (Exception e)
            {
                Log.ErrorFormat("Could not generate because of exception: {0}", new object[]{e});
                return;
            }

            Type type = res.CompiledAssembly.GetType(temporaryClassName);

            var gen = Activator.CreateInstance(type) as InlineTemplate;

            Generator = gen;
        }

        private string FixNormalOutputText(string text)
        {
            text = text.Replace("\"", "\"\"");
            return text;
        }

        private bool GetConfigArgument(string argumentSymbolName, string configMatch, out string argument, bool required = false)
        {
            const string configAssignmentSymbol = "=";
            const string configInfoRegexFormat = @".*{0}\s*{1}\s*(?<{0}>\S*).*";

            var configProviderRegex = new Regex(string.Format(configInfoRegexFormat, argumentSymbolName, configAssignmentSymbol));

            Match providerConfigMatch = configProviderRegex.Match(configMatch);

            if (providerConfigMatch.Success == false)
            {
                if (required)
                {
                    throw new InvalidDataException(string.Format("Config data did not specify '{0}' attribute. This is required.", argumentSymbolName));
                }

                // Value was not found but was also not required.
                argument = string.Empty;
                return false;
            }

            Group group = providerConfigMatch.Groups[argumentSymbolName];
            argument = group.Value;
            return true;
        }
    }
}