﻿namespace Shygen.Editor
{
    using System;
    using System.Collections.Generic;
    using Interfaces;

    /// <summary>
    /// Saves all information for all generation <see cref="DataDraft" /> in one
    /// <see cref="ICodeGenerationCategory" />. One category can have multiple entires.
    /// In the MVC (Model-View-Controller) example this class would save all
    /// data drafts of Models that have to be generated. When an data draft is selected an inspector
    /// is shown where potential properties can be modified. In our Example the Model could
    /// specify some variables that are generated as properties with automatic 'ModelUpdated()'
    /// method calls.
    /// </summary>
    [Serializable]
    public class Module
    {
        private string name = string.Empty;

        private List<DataDraft> dataDrafts = new List<DataDraft>();

        public List<DataDraft> DataDrafts
        {
            get { return dataDrafts; }
            set { dataDrafts = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }
    }
}