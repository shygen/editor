﻿#region Disclaimer

// <copyright file="CodeGenerationHelper.cs">
// Copyright (c) 2016 - 2017 All Rights Reserved
// </copyright>
// <author>Robin Fischer</author>

#endregion

using System.Reflection;
using System.Text;
using UnityEngine;

namespace Shygen.Editor
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using Attributes;
    using Generators;
    using Interfaces;
    using Newtonsoft.Json;
    using RobinBird.Logging.Runtime;
    using RobinBird.Utilities.Runtime.Extensions;
    using RobinBird.Utilities.Runtime.Helper;
    using Runtime.Attributes;
    using Runtime.CSharpGeneration;

    /// <summary>
    /// Helper methods to assist with the generation process.
    /// </summary>
    public static class CodeGenerationHelper
    {
        private const string ShygenTempDirectoryName = "ShygenTemp";
        private static ICodeGenerationCategory[] _cachedCodeGenerationCategories;
        private static Dictionary<ICodeGenerationCategory, PropertyType[]> _propertyTypeCache;

        public static ICodeGenerationEditorSettings generationEditorSettings;

        public static void GenerateFromCurrentAppDomain(string projectRootPath, string[] modules = null)
        {
            var types = AppDomain.CurrentDomain.GetTypes(typeof(object));
            GenerateFromTypes(projectRootPath, types, modules);
        }

        public class ModuleGeneration
        {
            public string Name;
            public List<ModuleDataType> ModuleDataTypes = new List<ModuleDataType>();
        }

        public class ModuleDataType
        {
            public string Name;
            public string Namespace;
            public GenerationDecorationAttribute[] Decorations;
            public GenerateAttribute Attribute;
            public Type CategoryRuntimeAttributeType;

            public string ModuleName => Attribute.ModuleName;
            public override string ToString()
            {
                return $"ModuleName: {ModuleName} Type: {Namespace}.{Name} Category: {CategoryRuntimeAttributeType}";
            }
        }

        public class CategoryGeneration
        {
            public string CategoryName;

            public Type CategoryType;

            /// <summary>
            /// Type of the skeleton which is needed for generation
            /// </summary>
            public Type DataDraftType;

            public Type RuntimeAttributeType;

            public List<TemplateConfiguration> TemplatePaths = new List<TemplateConfiguration>();
        }

        public class GroupGeneration
        {
            public string Name;
            public string Namespace;
            public TemplateConfiguration DestinationPath;
            public string[] ModuleNames;
            public Module[] Modules;
        }

        private static Dictionary<Type, CategoryGeneration> masterCategories;
        private static Stopwatch stopwatch;
        private static TimeSpan previousTime;


        private static CategoryGeneration GetCategoryForType(Type categoryType)
        {
            CategoryGeneration cat;
            if (masterCategories.TryGetValue(categoryType, out cat) == false)
            {
                cat = new CategoryGeneration();
                masterCategories.Add(categoryType, cat);
            }
            return cat;
        }

        private static CategoryGeneration GetCategoryForRuntimeAttributeType(Type attributeType)
        {
            foreach (var pair in masterCategories)
            {
                if (pair.Value.RuntimeAttributeType == attributeType)
                {
                    return pair.Value;
                }
            }
            return null;
        }

        private static JsonSerializerSettings jsonSettings
        {
            get
            {
                return new JsonSerializerSettings()
                {
                    TypeNameHandling = TypeNameHandling.Auto
                };
            }
        }

        public static void GenerateFromTypes(string projectRootPath, Type[] types, string[] modules = null)
        {
            stopwatch = new Stopwatch();
            stopwatch.Start();
            Log.Info("Starting to generate with " + types.Length + " types.");
            // Different types of generation categories
            masterCategories = new Dictionary<Type, CategoryGeneration>();
            var masterGroups = new Dictionary<string, GroupGeneration>();
            var masterModules = new Dictionary<string, ModuleGeneration>();
            var codeGenerationSettings = new CodeGenerationSettings();

            // Find all types that actually want to have generation support and add them to attribute structure
            // Also find group and generation configuration
            for (int i = 0; i < types.Length; i++)
            {
                var type = types[i];

                ExtractCategoryInformation(type);

                if (generationEditorSettings.UseRoslyn == false)
                {
                    // Find types that want generation by defining a category attribute
                    ExtractDatas(type, masterModules);

                    // Find group configuration
                    ExtractGroupConfigs(type, masterGroups);

                    // Find template configurations
                    ExtractTemplateConfigs(type);
                }
            }

            if (generationEditorSettings.UseRoslyn)
            {
                ExtractRoslynGroupConfigs(masterGroups);
                ExtractRoslynTemplateConfigs();
                ExtractRoslynData(projectRootPath, modules, masterGroups, masterModules);
            }

            InitializeCategorySettings(codeGenerationSettings);

            StopwatchStep("Extracted Types");

            Log.Info("Found groups: " + masterGroups.Count);
            foreach (var pair in masterGroups)
            {
                var group = pair.Value;
                Log.Info("Handling group: " + group.Name);

                group.Modules = new Module[group.ModuleNames.Length];

                // Initialize Modules
                for (int i = 0; i < group.ModuleNames.Length; i++)
                {
                    var moduleName = group.ModuleNames[i];

                    Log.Info("Handling Module: " + moduleName);
                    ModuleGeneration moduleGeneration;
                    if (masterModules.TryGetValue(moduleName, out moduleGeneration) == false)
                    {
                        continue;
                    }
                    var module = new Module();
                    group.Modules[i] = module;
                    module.Name = moduleGeneration.Name;

                    for (int j = 0; j < moduleGeneration.ModuleDataTypes.Count; j++)
                    {
                        ModuleDataType moduleDataType = moduleGeneration.ModuleDataTypes[j];
                        Log.Info("Handling Module Datatype: " + moduleDataType);
                        var category = GetCategoryForRuntimeAttributeType(moduleDataType.CategoryRuntimeAttributeType);

                        if (category.DataDraftType == null)
                        {
                            Log.Error("Category " + category.CategoryName + " did not provide a data draft type.");
                            continue;
                        }
                        // Create datadraft instances
                        var dataDraft = Activator.CreateInstance(category.DataDraftType);
                        var method = category.DataDraftType.GetMethod(nameof(DataDraft.Initialize));
                        if (method != null)
                        {
                            method.Invoke(dataDraft, new object[] { moduleDataType.Name, moduleDataType.Namespace, moduleDataType.Attribute, moduleDataType.Decorations });
                        }
                        else
                        {
                            Log.Error($"Could not find '{nameof(DataDraft.Initialize)} method via Reflection.");
                        }

                        Log.Info("Using DataDraftType: " + dataDraft.GetType());
                        module.DataDrafts.Add((DataDraft)dataDraft);
                    }
                }
            }

            StopwatchStep("Generating code");
            foreach (var pair in masterGroups)
            {
                GroupGeneration group = pair.Value;
                // Initialize Modules
                foreach (var module in group.Modules)
                {
                    if (module == null)
                    {
                        // Was not initialized so don't process
                        continue;
                    }

                    if (modules != null && modules.Length > 0 && modules.Contains(module.Name) == false)
                    {
                        // Skip generation because not wanted by user
                        continue;
                    }

                    Log.Info("Generate Module to: " + group.DestinationPath + ", with namespace: " + group.Namespace);
                    GenerateFiles(projectRootPath, module, codeGenerationSettings, group.Namespace, group.DestinationPath.Path, group.DestinationPath.Scope, group.Name);
                    StopwatchStep("Generated Module: " + group.Name);
                }
            }

            stopwatch.Stop();
            stopwatch = null;
        }

        public static void ExtractRoslynGroupConfigs(Dictionary<string, GroupGeneration> masterGroups)
        {
            ExtractRoslynGroupConfigs(GetConfigFilePath(), masterGroups);
        }

        public static void ExtractRoslynGroupConfigs(string configPath,
            Dictionary<string, GroupGeneration> masterGroups)
        {
            var syntaxWalkerResult = GetRoslynSyntaxData(configPath);

            if (syntaxWalkerResult == null)
            {
                Log.Error("Could not get group config roslyn data");
                return;
            }

            foreach (RoslynSyntaxFileData fileData in syntaxWalkerResult.Files)
            {
                foreach (RoslynSyntaxFileData.AttributeData attributeData in fileData.Attributes)
                {
                    if (attributeData.Type == typeof(GroupConfigurationAttribute))
                    {
                        var group = new GroupGeneration();
                        var moduleNames = new List<string>();
                        for (var i = 0; i < attributeData.Arguments.Length; i++)
                        {
                            RoslynSyntaxFileData.ArgumentData argumentData = attributeData.Arguments[i];

                            switch (i)
                            {
                                case 0:
                                    @group.Name = (string) argumentData.Value;
                                    break;
                                case 1:
                                    @group.Namespace = (string) argumentData.Value;
                                    break;
                                case 2:
                                    //@group.DestinationPath = (string) argumentData.Value;
                                    break;
                                default:
                                    moduleNames.Add((string) argumentData.Value);
                                    break;
                            }
                        }

                        @group.ModuleNames = moduleNames.ToArray();
                        masterGroups[@group.Name] = @group;
                    }
                }
            }
        }

        private static string GetConfigFilePath()
        {
            // TODO: Find a way to get this dynamically
            return Path.Combine(Application.dataPath, "BrewTycoon/Scripts/Editor/GenerationConfiguration.cs");

        }

        private static void ExtractRoslynTemplateConfigs()
        {
            var configPath = GetConfigFilePath();
            var syntaxWalkerResult = GetRoslynSyntaxData(configPath);

            if (syntaxWalkerResult == null)
            {
                Log.Error("Could not get group config roslyn data");
                return;
            }

            foreach (RoslynSyntaxFileData fileData in syntaxWalkerResult.Files)
            {
                foreach (RoslynSyntaxFileData.AttributeData attributeData in fileData.Attributes)
                {
                    if (attributeData.Type == typeof(TemplateConfigurationAttribute))
                    {
                        CategoryGeneration category = null;
                        for (var i = 0; i < attributeData.Arguments.Length; i++)
                        {
                            RoslynSyntaxFileData.ArgumentData argumentData = attributeData.Arguments[i];

                            switch (i)
                            {
                                case 0:
                                    TypeData typeData = (TypeData)argumentData.Value;
                                    Type categoryType = AppDomain.CurrentDomain.GetType(typeData.FullName);
                                    if (categoryType != null)
                                    {
                                        category = GetCategoryForType(categoryType);
                                    }

                                    if (category == null)
                                    {
                                        Log.Error($"Could not find category for {argumentData.Value}");
                                    }

                                    break;
                                default:
                                    if (category != null)
                                    {
                                        //category.TemplatePaths.Add((string) argumentData.Value);
                                    }

                                    break;
                            }
                        }
                    }
                }
            }
        }

        private static void ExtractRoslynData(string projectRootPath, string[] modules, Dictionary<string, GroupGeneration> masterGroups,
            Dictionary<string, ModuleGeneration> masterModules)
        {
            foreach (var pair in masterGroups)
            {
                var group = pair.Value;

                // Initialize Modules
                for (int i = 0; i < @group.ModuleNames.Length; i++)
                {
                    var moduleName = @group.ModuleNames[i];

                    if (modules != null && modules.Length > 0 && modules.Contains(moduleName) == false)
                    {
                        continue;
                    }

                    // string generationPath = Path.Combine(projectRootPath, @group.DestinationPath).Replace(@"\", "/");
                    // string generationModulePath = Path.Combine(generationPath, moduleName).Replace(@"\", "/");
                    //
                    // ExtractRoslynModules(generationModulePath, masterModules);
                }
            }
        }

        private static void StopwatchStep(string action)
        {
            var delta = stopwatch.Elapsed - previousTime;
            Log.Info($"<b>STOPWATCH</b> ({delta.TotalMilliseconds}){stopwatch.Elapsed}: {action}", category: "Stopwatch");
            previousTime = stopwatch.Elapsed;
        }

        public static DataDraft CreateDataDraft(Type categoryType)
        {
            var dataDraftAttribute = categoryType.GetCustomAttribute<DataDraftTypeAttribute>(false);

            if (dataDraftAttribute == null)
            {
                Log.Error("Could not find " + typeof(DataDraftTypeAttribute) + "on category '" + categoryType.Name + ". Every category must specify their " + typeof(DataDraft));
                return null;
            }

            var dataDraftType = dataDraftAttribute.Type;
            return (DataDraft) Activator.CreateInstance(dataDraftType);
        }

        private static void InitializeCategorySettings(CodeGenerationSettings codeGenerationSettings)
        {
            codeGenerationSettings.CategorySettings = new List<CodeGenerationSettings.CodeGenerationCategorySetting>();

            foreach (var pair in masterCategories)
            {
                var category = pair.Value;
                codeGenerationSettings.CategorySettings.Add(new CodeGenerationSettings.CodeGenerationCategorySetting()
                {
                    CategoryName = category.CategoryName,
                    Settings = category.TemplatePaths.ConvertAll(templatePath => new CodeGenerationSettings.CodeGenerationSetting() {Path = templatePath})
                });
            }
        }

        private static void ExtractTemplateConfigs(Type type)
        {
            var templateConfigAttributes = type.GetCustomAttributes(typeof(TemplateConfigurationAttribute), false);
            for (int j = 0; j < templateConfigAttributes.Length; j++)
            {
                TemplateConfigurationAttribute templateConfigAttribute = (TemplateConfigurationAttribute) templateConfigAttributes[j];

                Type categoryType = AppDomain.CurrentDomain.GetType(templateConfigAttribute.CategoryType.FullName);
                var generationCategoryType = typeof(ICodeGenerationCategory);
                if (generationCategoryType.IsAssignableFrom(categoryType) == false)
                {
                    Log.ErrorFormat("The category ({0}) defined in {1} is not inheriting from {2}", new object[]{ categoryType, templateConfigAttribute, generationCategoryType});
                    continue;
                }
                var category = GetCategoryForType(categoryType);

                for (int k = 0; k < templateConfigAttribute.TemplatePaths.Length; k++)
                {
                    category.TemplatePaths.Add(templateConfigAttribute.TemplatePaths[k]);
                }
            }
        }

        public static void ExtractGroupConfigs(Type type, Dictionary<string, GroupGeneration> masterGroups)
        {
            var scopeAttributes = type.GetCustomAttributes<ScopeConfigurationAttribute>(false);

            var groupConfigAttributes = type.GetCustomAttributes<GroupConfigurationAttribute>(false);
            for (int i = 0; i < groupConfigAttributes.Length; i++)
            {
                var groupConfigAttribute = groupConfigAttributes[i];
                if (groupConfigAttribute != null)
                {
                    for (int j = 0; j < scopeAttributes.Length; j++)
                    {
                        ScopeConfigurationAttribute scope = scopeAttributes[j];
                        if (scope.Group != groupConfigAttribute.GroupName)
                            continue;

                        var groupGeneration = new GroupGeneration();
                        groupGeneration.Name = groupConfigAttribute.GroupName;
                        groupGeneration.Namespace = groupConfigAttribute.Namespace;
                        groupGeneration.ModuleNames = groupConfigAttribute.ModuleNames;
                        groupGeneration.DestinationPath = new TemplateConfiguration(scope.Path, scope.Scope);
                        masterGroups.Add($"{groupGeneration.Name}_{scope.Scope}", groupGeneration);
                    }
                }
            }

        }

        private static void ExtractRoslynModules(string modulePath, Dictionary<string, ModuleGeneration> masterModules)
        {
            SyntaxWalkerResult syntaxData = GetRoslynSyntaxData(modulePath);

            if (syntaxData == null)
            {
                Log.Error("Could not parse code with Roslyn");
                return;
            }

            foreach (RoslynSyntaxFileData file in syntaxData.Files)
            {
                Log.Info($"Roslyn Found file: {file.Path}");
                string name = file.ClassName;
                string @namespace = file.Namespace;

                string moduleName = null;
                Type generateAttributeType = null;
                Type categoryType = null;
                List<GenerationDecorationAttribute> decorations = new List<GenerationDecorationAttribute>();
                foreach (var attribute in file.Attributes)
                {
                    Type attributeType = AppDomain.CurrentDomain.GetType(attribute.Type.FullName);

                    if (attributeType == null)
                    {
                        Log.Error($"Could not find attribute type for: {attribute.Type} in file: {file.Path}");
                        continue;
                    }

                    if (attributeType == typeof(GenerateAttribute))
                    {
	                    generateAttributeType = attributeType;
                        moduleName = (string)attribute.Arguments[0].Value;
                        var typeData = (TypeData) attribute.Arguments[1].Value;
                        categoryType = AppDomain.CurrentDomain.GetType(typeData.FullName);
                    }
                    else if (typeof(GenerationDecorationAttribute).IsAssignableFrom(attributeType))
                    {
                        var constructorTypes = new List<Type>();
                        var constructorValues = new List<object>();
                        foreach (RoslynSyntaxFileData.ArgumentData argument in attribute.Arguments)
                        {
                            Type argumentType = AppDomain.CurrentDomain.GetType(argument.Type.FullName);

                            if (argumentType == null)
                            {
                                Log.Error($"Could not find argument type for {argument.Type}");
                                continue;
                            }

                            constructorTypes.Add(argumentType);
                            if (argumentType.IsEnum)
                            {
                                constructorValues.Add(Enum.ToObject(argumentType, argument.Value));
                            }
                            else
                            {
                                constructorValues.Add(Convert.ChangeType(argument.Value, argumentType));
                            }
                        }


                        ConstructorInfo constructor = null;

                        ConstructorInfo[] constructorInfos = attributeType.GetConstructors();
                        foreach (ConstructorInfo info in constructorInfos)
                        {
                            ParameterInfo[] parameterInfos = info.GetParameters();
                            if (parameterInfos.Length < constructorTypes.Count)
                            {
                                // We are passing more parameters to the constructor so this one can not be it with less options than we pass
                                continue;
                            }
                            bool foundMatch = true;
                            for (int i = 0; i < constructorTypes.Count; i++)
                            {
                                var infoType = parameterInfos[i].ParameterType;
                                var constructorType = constructorTypes[i];
                                if (constructorType != null && constructorType != infoType)
                                {
                                    foundMatch = false;
                                    break;
                                }
                            }

                            if (foundMatch)
                            {
                                // Add optional arguments which have not been specified in the file
                                bool filledOptionals = true;
                                int constructorTypesCount = constructorTypes.Count;
                                for (int i = 0; i < parameterInfos.Length - constructorTypesCount; i++)
                                {
                                    var parameterInfo = parameterInfos[constructorTypesCount + i];
                                    // Only fill missing when there is actually a optional parameter. If not search further
                                    if (parameterInfo.IsOptional == false)
                                    {
                                        filledOptionals = false;
                                        break;
                                    }
                                    constructorValues.Add(Type.Missing);
                                }

                                if (filledOptionals)
                                {
                                    constructor = info;
                                    break;
                                }
                            }
                        }

                        if (constructor != null)
                        {
                            var decorationInstance = constructor.Invoke(constructorValues.ToArray());
                            decorations.Add((GenerationDecorationAttribute) decorationInstance);
                        }
                        else
                        {
                            Log.Error(
                                $"Could not find constructor decoration: {attributeType.FullName} in {file.Path}");
                        }
                    }
                }

                if (string.IsNullOrEmpty(moduleName) == false
                    && categoryType != null)
                {
                    var moduleDataType = new ModuleDataType();
                    moduleDataType.Name = name;
                    // TODO: Fix when looking at Roslyn again.
                    //moduleDataType.Attribute = generateAttributeType;
                    moduleDataType.Namespace = @namespace;
                    moduleDataType.CategoryRuntimeAttributeType = categoryType;
                    moduleDataType.Decorations = decorations.ToArray();
                    ModuleGeneration moduleGeneration;
                    if (masterModules.TryGetValue(moduleName, out moduleGeneration) == false)
                    {
                        moduleGeneration = new ModuleGeneration();
                        moduleGeneration.Name = moduleName;
                        masterModules.Add(moduleGeneration.Name, moduleGeneration);
                    }
                    moduleGeneration.ModuleDataTypes.Add(moduleDataType);
                }
                else
                {
                    Log.Info($"Ignoring file because not module has been detected: {file.Path}");
                }
            }
        }

        private static SyntaxWalkerResult GetRoslynSyntaxData(string modulePath)
        {
            Log.Info("Get Roslyn data at: " + modulePath);
            var processStartInfo = new ProcessStartInfo(generationEditorSettings.DotnetPath,$"{generationEditorSettings.AnalyzerPath} {modulePath}");
            processStartInfo.RedirectStandardOutput = true;
            processStartInfo.RedirectStandardError = true;
            processStartInfo.UseShellExecute = false;
            var process = Process.Start(processStartInfo);

            if (process == null)
            {
                Log.Error("Could not start Roslyn process");
                return null;
            }

            process.WaitForExit();

            var result = process.StandardOutput.ReadToEnd();

            var error = process.StandardError.ReadToEnd();
            if (string.IsNullOrEmpty(error) == false)
            {
                Log.Error($"Proess had error: {error}");
            }

            Log.Info("Got result: " + result);

            return JsonConvert.DeserializeObject<SyntaxWalkerResult>(result, jsonSettings);
        }


        private static void ExtractDatas(Type type, Dictionary<string, ModuleGeneration> masterModules)
        {
            var generateAttribute = type.GetCustomAttribute<GenerateAttribute>(false);
            if (generateAttribute != null)
            {
                var dataType = new ModuleDataType();

                dataType.Attribute = generateAttribute;
                dataType.CategoryRuntimeAttributeType = generateAttribute.CategoryAttributeType;
                dataType.Name = type.Name;
                dataType.Namespace = type.Namespace;
                dataType.Decorations = type.GetCustomAttributes<GenerationDecorationAttribute>().ToArray();

                ModuleGeneration moduleGeneration;
                if (masterModules.TryGetValue(dataType.ModuleName, out moduleGeneration) == false)
                {
                    moduleGeneration = new ModuleGeneration();
                    moduleGeneration.Name = dataType.ModuleName;
                    masterModules.Add(moduleGeneration.Name, moduleGeneration);
                }
                moduleGeneration.ModuleDataTypes.Add(dataType);
            }
        }

        private static void ExtractCategoryInformation(Type categoryType)
        {
            if (categoryType.IsAbstract
                || categoryType == typeof(ICodeGenerationCategory)
                || typeof(ICodeGenerationCategory).IsAssignableFrom(categoryType) == false)
            {
                return;
            }

            var dataDraftAttribute = categoryType.GetCustomAttribute<DataDraftTypeAttribute>(false);
            if (dataDraftAttribute == null)
            {

                Log.Error("Could not find " + typeof(DataDraftTypeAttribute) + " for " + categoryType);
                return;
            }

            var runtimeAttributeType = categoryType.GetCustomAttribute<RuntimeAttributeTypeAttribute>(false);
            if (runtimeAttributeType == null)
            {
                Log.Error("Could not find " + typeof(RuntimeAttributeTypeAttribute) + " for " + categoryType);
                return;
            }

            var displayNameAttribute = categoryType.GetCustomAttribute<DisplayNameAttribute>(false);
            if (displayNameAttribute == null)
            {
                Log.Error("Could not find " + typeof(DisplayNameAttribute) + " for " + categoryType);
                return;
            }

            var category = GetCategoryForType(categoryType);
            category.CategoryType = categoryType;
            category.RuntimeAttributeType = runtimeAttributeType.Type;
            category.DataDraftType = dataDraftAttribute.Type;
            category.CategoryName = displayNameAttribute.DisplayName;
        }

        public static void GenerateFiles(string projectRootPath, Module module, CodeGenerationSettings settings, CodeGenerationSettings.ModuleGroupSetting group)
        {
            stopwatch = new Stopwatch();
            stopwatch.Start();
            GenerateFiles(projectRootPath, module, settings, group.Namespace, group.TargetDirectoryPath, group.Scope, group.Name);
            stopwatch.Stop();
            stopwatch = null;
        }
        
        public static void GenerateFiles(string projectRootPath, Module module, CodeGenerationSettings settings, string groupNamespace, string destinationPath, string scope, string groupName)
        {
            if (module == null)
            {
                Log.Error("Cannot generate files with null module");
                return;
            }

            string generationPath = Path.Combine(projectRootPath, destinationPath).Replace(@"\", "/");
            generationPath = Path.Combine(generationPath, "GeneratedCode");
            string generationModulePath = Path.Combine(generationPath, module.Name).Replace(@"\", "/");

            // Create a backup copy from the module.
            if (Directory.Exists(generationModulePath))
            {
                string copyPath = Path.Combine(Path.Combine(projectRootPath, ShygenTempDirectoryName), $"{module.Name}_{scope}");
                if (Directory.Exists(copyPath))
                {
                    // Recreate the directory
                    Directory.Delete(copyPath, true);
                }
                Directory.CreateDirectory(copyPath);
                DirectoryHelper.CopyDirectory(generationModulePath, copyPath);
            }

            StopwatchStep("Create backup directory");

            InitUniqueIdProvider();

            int catCount = GetCodeGenerationCategories().Length;
            for (var i = 0; i < catCount; i++)
            {
                ICodeGenerationCategory cat = GetCodeGenerationCategories()[i];
                DataDraft[] dataDrafts = GetDataDraftsForCategory(cat, module);

                if (dataDrafts == null || dataDrafts.Length == 0)
                {
                    continue;
                }

                string moduleNamespace = $"{groupNamespace}.{module.Name}";


                CodeGenerationSettings.CodeGenerationCategorySetting categorySettings = settings.GetSettingsForCategory(cat.Name);

                if (categorySettings == null)
                {
                    continue;
                }

                bool hasCreatedDirectory = false;

                for (var j = 0; j < categorySettings.Settings.Count; j++)
                {
                    CodeGenerationSettings.CodeGenerationSetting setting = categorySettings.Settings[j];

                    if (setting.Path.Scope != scope)
                    {
                        continue;
                    }

                    var categoryGenerationPath = Path.Combine(generationModulePath, cat.Name);
                    if (hasCreatedDirectory == false)
                    {
                        CleanCategoryDirectory(categoryGenerationPath);
                        hasCreatedDirectory = true;
                    }

                    var file = new FileInfo(Path.Combine(projectRootPath, setting.Path.Path));

                    if (file.Exists == false)
                    {
                        Log.ErrorFormat("Could not find Template at relative path: {0} for category: {1}", new object[]{ setting.Path, cat.Name});
                    }

                    StopwatchStep("Starting to get generator");
                    var generators = Generator.GetGeneratorsForTemplate(file);
                    foreach (Generator generator in generators)
                    {
                        StopwatchStep("Get generator");
                        // TODO: Improve Inline generator performance. Takes to much time.
                        // TODO: This initialization is done for every module. Caching is required
                        generator.Initialize(file);
                        StopwatchStep("Initialized generator");

                        GenerateCodeFromGenerators(module.Name, moduleNamespace, cat, generator, categoryGenerationPath, dataDrafts, scope, groupName);
                        StopwatchStep("Generated file: " + file.FullName);
                        generator.Dispose();
                    }
                }
            }
            WriteUniqueIdProvider();
        }
        
        private static Dictionary<string, string> componentIdentifier = new();
        private static string IdPath => Path.Combine(Application.dataPath, "Game/Misc/CodeGeneration/ComponentIds.json");

        private static void InitUniqueIdProvider()
        {
            componentIdentifier.Clear();
            if (File.Exists(IdPath))
            {
                var idText = File.ReadAllText(IdPath, Encoding.UTF8);
                var deserializeObject = JsonConvert.DeserializeObject<Dictionary<string, string>>(idText);
                foreach (var pair in deserializeObject)
                {
                    if (componentIdentifier.TryAdd(pair.Key, pair.Value) == false)
                    {
                        Log.Error($"Already got an duplicate Id from file: {pair.Key}");
                    }
                }
            }
        }

        private static void WriteUniqueIdProvider()
        {
            var fileInfo = new FileInfo(IdPath);
            if (fileInfo.Directory?.Exists == false)
            {
                fileInfo.Directory.Create();
            }
            var json = JsonConvert.SerializeObject(componentIdentifier, Formatting.Indented);
            File.WriteAllText(IdPath, json, Encoding.UTF8);
        }
        
        public static string GetUniquePersistedId(string name, string moduleNamespace)
        {
            var key = $"{moduleNamespace}.{name}";
            if (componentIdentifier.TryGetValue(key, out var savedId))
            {
                return savedId;
            }
            var uppercaseChars = name.Where(char.IsUpper).Take(4).ToArray();
            if (uppercaseChars.Length < 4)
            {
                var newArray = new[]{'_','_','_','_'};
                uppercaseChars.CopyTo(newArray, 0);
                uppercaseChars = newArray;
            }
            string newIdentifier = new string(uppercaseChars);

            int variationCounter = 1;
            while (componentIdentifier.ContainsValue(newIdentifier))
            {
                uppercaseChars[3] = (char)('0' + variationCounter++);
                newIdentifier = new string(uppercaseChars);
            }
            componentIdentifier.Add(key, newIdentifier);
            return newIdentifier;
        }

        private static bool CleanCategoryDirectory(string categoryGenerationPath)
        {
            try
            {
                if (Directory.Exists(categoryGenerationPath))
                {
                    Directory.Delete(categoryGenerationPath, true);
                }

                Directory.CreateDirectory(categoryGenerationPath);
                return true;
            }
            catch (Exception)
            {
                Log.ErrorFormat("Could not delete and/or create output directory at: {0}", new object[]{ categoryGenerationPath});
            }

            return false;
        }

        public static DataDraft[] GetDataDraftsForCategory(ICodeGenerationCategory cat, Module module)
        {
            var dataDraftAttribute = cat.GetType().GetCustomAttribute<DataDraftTypeAttribute>(false);

            if (dataDraftAttribute == null)
            {
                Log.Error("Could not find " + typeof(DataDraftTypeAttribute) + "on category '" + cat.Name + ". Every category must specify their " + typeof(DataDraft));
                return new DataDraft[0];
            }

            var dataDraftType = dataDraftAttribute.Type;

            var filteredDrafts = new List<DataDraft>();
            for (int i = 0; i < module.DataDrafts.Count; i++)
            {
                var dataDraft = module.DataDrafts[i];
                if (dataDraft.GetType() == dataDraftType)
                {
                    filteredDrafts.Add(dataDraft);
                }
            }
            return filteredDrafts.ToArray();
        }

        private static void GenerateCodeFromGenerators(string moduleName, string moduleNamespace, ICodeGenerationCategory category, Generator generator, string generationPath, DataDraft[] datas, string scope, string group)
        {
            Generator.GeneratedCodeResult[] generatedCodeResults;

            generator.Generate(moduleName, moduleNamespace, category, datas, scope, group, out generatedCodeResults);

            StopwatchStep("Generator generated");

            for (var j = 0; j < generatedCodeResults.Length; j++)
            {
                Generator.GeneratedCodeResult genCode = generatedCodeResults[j];
                string filename = Path.Combine(generationPath, genCode.FileName);
                string filePath = filename;
                if (Path.HasExtension(filename) == false)
                {
                    filePath = $"{filename}{generator.Extension}";
                }

                using (var writer = new StreamWriter(filePath))
                {
                    writer.Write(genCode.Code);
                }
            }
            StopwatchStep("Files written");
        }

        public static string GetUniquePropertyName<T>(string baseName, List<T> existingNames, Func<T, string> getNameFunc)
        {
            string newName;
            var counter = 0;
            while (true)
            {
                var newNameValid = true;
                newName = string.Format("{0}{1}", baseName, counter > 0 ? string.Format("_{0}", counter) : string.Empty);
                for (var i = 0; i < existingNames.Count; i++)
                {
                    T item = existingNames[i];

                    if (getNameFunc(item) == newName)
                    {
                        newNameValid = false;
                        break;
                    }
                }

                if (newNameValid)
                {
                    break;
                }
                counter++;
            }
            return newName;
        }

        public static ICodeGenerationCategory[] GetCodeGenerationCategories()
        {
            if (_cachedCodeGenerationCategories == null)
            {
                Type[] categoryTypes = AppDomain.CurrentDomain.GetTypes(typeof (ICodeGenerationCategory));
                var tmpCategories = new List<ICodeGenerationCategory>();
                for (var i = 0; i < categoryTypes.Length; i++)
                {
                    Type catType = categoryTypes[i];
                    if (catType == null || catType.IsAbstract)
                    {
                        continue;
                    }
                    var instance = Activator.CreateInstance(catType) as ICodeGenerationCategory;
                    tmpCategories.Add(instance);
                }
                _cachedCodeGenerationCategories = tmpCategories.ToArray();
            }

            return _cachedCodeGenerationCategories;
        }
    }
}