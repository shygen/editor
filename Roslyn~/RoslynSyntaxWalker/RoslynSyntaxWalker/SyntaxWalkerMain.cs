﻿using System.Threading.Tasks;
using static System.Console;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.MSBuild;
using RobinBird.Utilities.Runtime.Extensions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using Shygen.Runtime.Attributes;
using Shygen.Runtime.CSharpGeneration;

namespace RoslynSyntaxWalker
{
    class SyntaxWalkerMain
    {
        public static SymbolDisplayFormat SymbolFormat { get; } =
            new SymbolDisplayFormat(
                globalNamespaceStyle: SymbolDisplayGlobalNamespaceStyle.Included,
                typeQualificationStyle: SymbolDisplayTypeQualificationStyle.NameAndContainingTypesAndNamespaces,
                genericsOptions: SymbolDisplayGenericsOptions.IncludeTypeParameters,
                miscellaneousOptions:
                SymbolDisplayMiscellaneousOptions.EscapeKeywordIdentifiers
                );

        static void Main(string[] args)
        {
            var task = Task.Run(async () =>
            {
                string basePath = args[0];
                
                var result = new SyntaxWalkerResult();
                var fileData = new List<RoslynSyntaxFileData>();
                
                var directoryInfo = new DirectoryInfo(basePath);
                var fileInfo = new FileInfo(basePath);
                
                Environment.SetEnvironmentVariable("MSBUILD_EXE_PATH", @"/usr/local/share/dotnet/sdk/3.1.102/MSBuild.dll");
                
                var msWorkspace = MSBuildWorkspace.Create();
                
                msWorkspace.WorkspaceFailed += (sender, eventArgs) =>
                {
                    Console.WriteLine($"Had error: {eventArgs.Diagnostic}");
                };


                Solution solution = await msWorkspace.OpenSolutionAsync("/Users/jenkins/Projects/BrewTycoon/Unity/Unity.sln");
                
                if (directoryInfo.Exists)
                {
                    foreach (FileInfo file in directoryInfo.GetFiles("*.cs", SearchOption.AllDirectories))
                    {
                        RoslynSyntaxFileData directoryFileData = await ExtractFileData(solution, file);
                        if (directoryFileData != null)
                        {
                            fileData.Add(directoryFileData);
                        }
                    }
                }
                else if (fileInfo.Exists)
                {
                    RoslynSyntaxFileData fileInfoData = await ExtractFileData(solution, fileInfo);
                    if (fileInfoData != null)
                    {
                        fileData.Add(fileInfoData);
                    }
                }
                else
                {
                    PrintUsages();
                }

                result.Files = fileData.ToArray();

                JsonSerializerSettings settings = new JsonSerializerSettings();
                settings.TypeNameHandling = TypeNameHandling.Auto;
                Write(JsonConvert.SerializeObject(result, settings));
            
            });

            task.Wait();
        }

        private static async Task<RoslynSyntaxFileData> ExtractFileData(Solution solution, FileInfo file)
        {
            var documentIds = solution.GetDocumentIdsWithFilePath(file.FullName);

            if (documentIds.Length == 0 || documentIds.Length > 1)
            {
                WriteLine($"ERROR: Found multiple or none documents for file: {file.FullName}");
                return null;
            }

            var documentId = documentIds[0];

            Document document = solution.GetDocument(documentId);
            
            var result = await ExtractFileData(document);
            if (result != null)
            {
                result.Path = file.FullName;
            }
            return result;
        }

        private static void PrintUsages()
        {
            WriteLine("ERROR: Invalid arguments. Usage:");
            WriteLine("syntaxwalker <basePath>");
            WriteLine("basePath - Directory or File path from where to extract information about generation.");
        }

        private static async Task<RoslynSyntaxFileData> ExtractFileData(Document document)
        {
            //WriteLine($"Check file: {document.FilePath}");
            var result = new RoslynSyntaxFileData();
            var attributeDataList = new List<RoslynSyntaxFileData.AttributeData>();

            SyntaxNode root = await document.GetSyntaxRootAsync();
            IEnumerable<ClassDeclarationSyntax> classes = root.DescendantNodes().OfType<ClassDeclarationSyntax>();

            foreach (ClassDeclarationSyntax classSyntax in classes)
            {
                // Filter a bit to not analyze all classes with attributes
                if (classSyntax.DescendantNodes()
                        .OfType<AttributeSyntax>().Any(syntax =>
                        {
                            return syntax.Name.ToString().Contains(CutoffAttribute(nameof(GenerateAttribute)))
                            || syntax.Name.ToString().Contains(CutoffAttribute(nameof(TemplateConfigurationAttribute)))
                            || syntax.Name.ToString().Contains(CutoffAttribute(nameof(GroupConfigurationAttribute)));
                        }) == false)
                {
                    continue;
                }

                SemanticModel model = await document.GetSemanticModelAsync();

                foreach (AttributeListSyntax attributeList in classSyntax.AttributeLists)
                {
                    foreach (AttributeSyntax attribute in attributeList.Attributes)
                    {
                        var attributeData = new RoslynSyntaxFileData.AttributeData();
                        var argumentDatas = new List<RoslynSyntaxFileData.ArgumentData>();

                        if (attribute.ArgumentList == null)
                        {
                            continue;
                        }

                        var attributeType = model.GetTypeInfo(attribute);

                        attributeData.Type = new TypeData(GetTypeString(attributeType.Type),
                            attributeType.Type.TypeKind == TypeKind.Enum,
                            attributeType.Type.TypeKind == TypeKind.Class);

                        foreach (AttributeArgumentSyntax argument in attribute.ArgumentList.Arguments)
                        {
                            var argumentData = new RoslynSyntaxFileData.ArgumentData();

                            var argumentType = model.GetTypeInfo(argument.Expression);

                            if (argument.NameColon != null)
                            {
                                WriteLine($"ERROR: named parameters are not supported at the moment. You are free to open a pull request :). File: {classSyntax.ToString()} attribute: {attribute.ToString()}");
                                return null;
                            }
                            
                            if (argument.Expression is TypeOfExpressionSyntax typeOfExpressionSyntax)
                            {
                                var typeofType = model.GetTypeInfo(typeOfExpressionSyntax.Type);
                                Type type = typeof(TypeData);
                                argumentData.Type = new TypeData(type.FullName, type.IsEnum, type.IsClass);
                                argumentData.Value = new TypeData($"{GetTypeString(typeofType.Type.ContainingNamespace)}.{typeofType.Type.Name}",
                                    typeofType.Type.TypeKind == TypeKind.Enum,
                                    typeofType.Type.TypeKind == TypeKind.Class);
                            }
                            else
                            {
                                argumentData.Type = new TypeData(GetTypeString(argumentType.Type),
                                    argumentType.Type.TypeKind == TypeKind.Enum,
                                    argumentType.Type.TypeKind == TypeKind.Class);
                                Optional<object> constantValue = model.GetConstantValue(argument.Expression);
                                argumentData.Value = constantValue.Value;
                            }

                            argumentDatas.Add(argumentData);
                        }

                        attributeData.Arguments = argumentDatas.ToArray();
                        attributeDataList.Add(attributeData);
                    }
                }

                INamedTypeSymbol classType = model.GetDeclaredSymbol(classSyntax);
                result.Namespace = GetTypeString(classType.ContainingNamespace);
                result.ClassName = classType.Name;
                result.Attributes = attributeDataList.ToArray();
                
                return result;
            }

            return null;
        }

        private static string GetTypeString(ISymbol symbol)
        {
            var type =  symbol.ToDisplayString(SymbolFormat).Replace("global::", string.Empty);
            if (symbol.ContainingType != null)
            {
                var dotIndex = type.LastIndexOf(".", StringComparison.Ordinal);
                type = type.ReplaceAt(dotIndex, '+');
            }

            return type;
        }

        private static string CutoffAttribute(string generateAttributeName)
        {
            var attributeCutOff = "Attribute";
            generateAttributeName = generateAttributeName.Remove(
                generateAttributeName.LastIndexOf(attributeCutOff, StringComparison.Ordinal),
                attributeCutOff.Length);
            return generateAttributeName;
        }
    }
}
