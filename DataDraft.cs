namespace Shygen.Editor
{
    using System;
    using JetBrains.Annotations;
    using RobinBird.Logging.Runtime;
    using RobinBird.Utilities.Runtime.Extensions;
    using Runtime.Attributes;

    /// <summary>
    /// Base class for every data draft. Inherit from this
    /// to implement custom data drafts with as much information
    /// you like. Information is passed to the templates to generate
    /// the final resulting file
    /// </summary>
    public abstract class DataDraft
    {
        private string name = string.Empty;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string Namespace { get; set; }


        public override string ToString()
        {
            return Name;
        }

        public virtual void Initialize(string name, string @namespace, GenerateAttribute generateAttribute, GenerationDecorationAttribute[] decorations)
        {
            if (name[0] == 'I' && Char.IsUpper(name[1]))
            {
                // If we are dealing with an interface reduce it to its normal form. Makes generating much easier
                name = name.Remove(0, 1);
            }
            Name = name;
            Namespace = @namespace;

            ProcessGenerateAttribute(generateAttribute);

            if (decorations != null)
            {
                foreach (GenerationDecorationAttribute decoration in decorations)
                {
                    ProcessDecoration(decoration);
                }
            }
        }

        protected virtual void ProcessDecoration(GenerationDecorationAttribute decoration)
        {
        }

        protected virtual void ProcessGenerateAttribute(GenerateAttribute attribute)
        {

        }
    }
}